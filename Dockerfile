# Select base image
FROM ruby:2.6.3
ARG url=https://bitbucket.org/darroue/smaps/get/development.zip
ARG tempfile=temp.zip
ARG bundler_install="bundle install --without=development test"

# Change working directory
WORKDIR /usr/src/app/web

# Download Gemfile and Gemfile.lock
RUN wget $url -q -O $tempfile \
    && unzip -j "$PWD/$tempfile" "*/web/Gemfile" "*/web/Gemfile.lock" -d "$PWD" \
    && rm -f $tempfile \
    # Update system
    && apt-get update -qq && apt-get upgrade -qq \
    # Set timezone to Prague
    && ln -fs /usr/share/zoneinfo/Europe/Prague /etc/localtime \
    # Install GEMs
    && $bundler_install
# Expose port 3000
EXPOSE 3000

# These commands are run on every start of docker container.
CMD \
    # Remove previous PIDs
    rm -rf tmp/pids \
    # Install GEMs (Server can be used with old Docker image)
    && $bundler_install \
    # Prepare Database
    && bundle exec rails \
        db:migrate \
        # Prepare JavaScript Assets
        assets:precompile \
    # Finally, start server
    && bundle exec rails s -b 0.0.0.0
